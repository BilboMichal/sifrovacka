<?php
declare(strict_types=1);

namespace Libs\Stringy\Latte;

use InvalidArgumentException;
use ReflectionClass;
use ReflectionMethod;
use Libs\Stringy\Stringy;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class StringyFilters
{

    public static function install(object $filterObj)
    {
        if (!method_exists($filterObj, 'addFilter')) {
            throw new InvalidArgumentException('Given $filterObj object must have addFilter(string $name, callable $filter) method');
        }

        $reflection = new ReflectionClass(Stringy::class);
        $methods = $reflection->getMethods(ReflectionMethod::IS_PUBLIC);
        foreach ($methods as $method) {
            if (substr($method->name, 0, 2) !== '__' && !in_array($method->name, ['getIterator'], true)) {
                $methodName = $method->name;
                $filterObj->addFilter($methodName,
                    function (string $str, ...$args) use ($methodName) {
                        return Stringy::create($str)->$methodName(...$args);
                    });
            }
        }

        $filterObj->addFilter('stringy',
            function (string $str): Stringy {
                return Stringy::create($str);
            });
    }
}