<?php
declare(strict_types=1);

namespace Libs\Stringy;

use Stringy\Stringy as StringyOriginal;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Stringy extends StringyOriginal
{

    public function spaceize(): self
    {
        return $this->delimit(' ');
    }
}