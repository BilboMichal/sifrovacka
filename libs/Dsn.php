<?php
declare(strict_types=1);

namespace Libs;

/**
 * Objektová reprezentace DSN řetězce
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Dsn
{
    /** @var string */
    private $dsn;

    /** @var array @internal */
    private $config;

    public function __construct(string $dsn)
    {
        $this->dsn = $dsn;
    }

    private function parse()
    {
        $config = parse_url($this->dsn);
        if (isset($config['path'])) {
            $config['path'] = trim($config['path'], '/');
        }

        $this->config = $config;
    }

    public function get(string $key)
    {
        if (!isset($this->config)) {
            $this->parse();
        }

        return $this->config[$key];
    }

    public function __get(string $key)
    {
        return $this->get($key);
    }
}