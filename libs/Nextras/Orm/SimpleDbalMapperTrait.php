<?php

namespace Libs\Nextras\Orm;

use Nextras\Orm\Mapper\Dbal\DbalMapper;

/**
 * Trait with simple DB mapper logic
 *
 * Use for mappers extended from
 * @see DbalMapper
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
trait SimpleDbalMapperTrait
{

    /**
     * Set mapper table name
     * $tableName should be considered immutable after mapper creation!
     *
     * @param string $tableName
     * @return $this
     */
    public function setTableName(string $tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }
}