<?php

namespace Libs\Nextras\Orm;

use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Entity\Reflection\EntityMetadata;
use Nextras\Orm\InvalidArgumentException;
use Nextras\Orm\Repository\Repository;

/**
 * Trait with simple repository logic
 * 
 * Use for repositories extended from
 * @see Repository
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
trait SimpleRepositoryTrait
{

    abstract public function getEntityClassName(array $data): string;

    public function onBeforeUpdate(IEntity $entity): void
    {
        $givenEntityClass = get_class($entity);
        if ($givenEntityClass !== $this->entityClassName) {
            throw new InvalidArgumentException(
                sprintf('Repository is for persisting entity of class "%s", instance of "%s" given instead',
                    $this->entityClassName, $givenEntityClass));
        }

        parent::onBeforeUpdate($entity);
    }

    /**
     * Set entity class name
     * $entityClassName should be considered immutable after repository creation!
     *
     * @param string $entityClassName
     * @return $this
     */
    public function setEntityClassName(string $entityClassName)
    {
        $this->entityClassName = $entityClassName;
        return $this;
    }

    public function getEntityMetadata(string $entityClass = null): EntityMetadata
    {
        if ($entityClass !== null) {
            return parent::getEntityMetadata($entityClass);
        } else {
            return $this->getModel()->getMetadataStorage()->get($this->entityClassName ?: static::getEntityClassNames()[0]);
        }
    }
}