<?php

namespace Libs\Nextras\Orm;

/**
 * This trait is using static property filled dynamically
 *
 * This trait is recomended for use in generic/anonymous classes only!
 * For regular repository class, please follow the Nextras ORM documentation
 * @link https://nextras.org/orm/docs/3.1/repository
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
trait GenericRepositoryTrait
{
    /** @var array */
    private static $entityClassNames = [];

    /**
     *
     * @param array $entityClassNames
     */
    public static function setEntityClassNames(array $entityClassNames)
    {
        self::$entityClassNames = array_values($entityClassNames);
    }

    /**
     *
     * @return array
     */
    public static function getEntityClassNames(): array
    {
        return self::$entityClassNames;
    }
}