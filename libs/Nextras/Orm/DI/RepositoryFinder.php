<?php

namespace Libs\Nextras\Orm\DI;

use Libs\Nextras\Orm\GenericRepositoryTrait;
use Libs\Nextras\Orm\SimpleDbalMapperTrait;
use Libs\Nextras\Orm\SimpleRepositoryTrait;
use DirectoryIterator;
use Nette\Caching\Cache;
use Nette\DI\Container;
use Nette\DI\ContainerBuilder;
use Nette\DI\Definitions\ServiceDefinition;
use Nette\DI\Definitions\Statement;
use Nette\DI\NotAllowedDuringResolvingException;
use Nextras\Dbal\IConnection;
use Nextras\Orm\Bridges\NetteDI\IRepositoryFinder;
use Nextras\Orm\Bridges\NetteDI\OrmExtension;
use Nextras\Orm\Bridges\NetteDI\RepositoryLoader;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Mapper\Dbal\DbalMapper;
use Nextras\Orm\Mapper\Dbal\DbalMapperCoordinator;
use Nextras\Orm\Mapper\IMapper;
use Nextras\Orm\Repository\IDependencyProvider;
use Nextras\Orm\Repository\IRepository;
use Nextras\Orm\Repository\Repository;
use Nextras\Orm\StorageReflection\StringHelper;
use ReflectionClass;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class RepositoryFinder implements IRepositoryFinder
{
    /** @var ContainerBuilder */
    private $builder;

    /** @var OrmExtension */
    private $extension;

    /**
     *
     * @param string $modelClass
     * @param ContainerBuilder $containerBuilder
     * @param OrmExtension $extension
     */
    public function __construct(string $modelClass, ContainerBuilder $containerBuilder, OrmExtension $extension)
    {
        $this->builder   = $containerBuilder;
        $this->extension = $extension;
    }

    /**
     *
     * @param string $pattern
     * @param string $value
     * @return string
     */
    final protected static function replaceWildcard(string $pattern, string $value): string
    {
        return str_replace('*', $value, $pattern);
    }

    /**
     * Override this method to provide Finder configuration from different place
     *
     * @return array
     */
    protected function getConfig(): array
    {
        return $this->builder->parameters['repositoryFinder'];
    }

    /**
     * Creates default repository for given entity class
     *
     * Override this method for extension
     *
     * @param string $entityClass
     * @param IMapper $mapper Mapper for this repository
     * @param Container $container
     * @return IRepository
     */
    public static function createRepository(string $entityClass, IMapper $mapper, Container $container): IRepository
    {
        $repository = new class($mapper, $container->getByType(IDependencyProvider::class, false)) extends Repository {

            use SimpleRepositoryTrait;
            use GenericRepositoryTrait;
        };
        return $repository->setEntityClassName($entityClass);
    }

    /**
     * Creates default mapper for given name
     *
     * @param string $name Mapper name - can be used for DB table for instance
     * @param Cache $cache
     * @param Container $container
     * @return IMapper
     */
    public static function createMapper(string $name, Cache $cache, Container $container): IMapper
    {
        $mapper = new class(
        $container->getByType(IConnection::class), $container->getByType(DbalMapperCoordinator::class), $cache
        ) extends DbalMapper {

            use SimpleDbalMapperTrait;
        };
        return $mapper->setTableName(StringHelper::underscore($name));
    }

    /**
     * Searches for entity classes for which services (repositories, mappers) will be registered
     *
     * @param array $config
     * @return array
     * @throws NotAllowedDuringResolvingException
     */
    protected function createEntitiesMapTable(array & $config): array
    {
        $entitiesMapTable = [];
        foreach ((array) $config['entityFiles'] as $entityDir) {
            $entityDirIterator = new DirectoryIterator($entityDir);
            foreach ($entityDirIterator as $entityFile) {
                if ($entityFile->isFile() && $entityFile->getExtension() === 'php') {
                    $entityName      = $entityFile->getBasename('.php');
                    $entityClass     = self::replaceWildcard($config['entityClasses'], $entityName);
                    $classReflection = new ReflectionClass($entityClass);

                    if ($classReflection->isInstantiable()) {
                        if (!$classReflection->implementsInterface(IEntity::class)) {
                            throw new NotAllowedDuringResolvingException(sprintf('Found entity class "%s" must implement "%s" interface',
                                    $entityClass, IEntity::class));
                        }

                        $entitiesMapTable[$entityName] = $entityClass;
                    }
                }
            }
        }

        return $entitiesMapTable;
    }

    /**
     * Builds default mapper definition
     *
     * Override this method for extension
     *
     * @param array $config
     * @return ServiceDefinition
     */
    protected function createMapperDefinition(array & $config): ServiceDefinition
    {
        extract($config);
        $cacheDefinition = new Statement($this->extension->prefix('@cache').'::derive', ['mapper']);

        $mapperDefinition = new ServiceDefinition;
        if (class_exists($mapperClass)) {
            $mapperDefinition->setType($mapperClass)
                ->setArguments([
                    'cache' => $cacheDefinition,
                ])
                // Autowiring is enabled - unique mapper class exists
                ->setAutowired(true);
        } else {
            $mapperDefinition->setFactory(get_class($this).'::createMapper')
                ->setArguments([
                    'name' => $entityNameLower,
                    'cache' => $cacheDefinition,
                ])
                ->setAutowired(false);
        }

        return $mapperDefinition;
    }

    /**
     * Builds default repository definition
     *
     * Override this method for extension
     *
     * @param array $config
     * @return ServiceDefinition
     */
    protected function createRepositoryDefinition(array & $config): ServiceDefinition
    {
        extract($config);
        $config['mapperClass'] = $mapperClass           = self::replaceWildcard($config['mapperClasses'], $entityName);

        $mapperServiceName = $this->builder->getByType($mapperClass);
        if (!$mapperServiceName) {
            // Mapper NOT registered in DI - lets create default one...
            $mapperServiceName = $this->extension->prefix('mapper.'.$entityNameLower);
            $mapperDefinition  = $this->createMapperDefinition($config);
            $this->builder->addDefinition($mapperServiceName, $mapperDefinition);
        } else {
            // Mapper already registered in DI
            $mapperDefinition = $this->builder->getDefinition($mapperServiceName);
        }

        $repositoryDefinition = new ServiceDefinition;
        if (class_exists($repositoryClass)) {
            $repositoryDefinition->setType($repositoryClass);
            // Autowiring is enabled - unique repository class exists

            $repositoryClassImplements = class_implements($repositoryClass);
            if (!isset($repositoryClassImplements[IRepository::class])) {
                throw new NotAllowedDuringResolvingException(sprintf('Repository class "%s" must implement "%s" interface',
                        $repositoryClass, IRepository::class));
            }

            $repositoryClassParents   = class_parents($repositoryClass);
            $repositoryClassParents[] = $repositoryClass;
            // Standard Nextras repository with mapper
            if (in_array(Repository::class, $repositoryClassParents, true)) {
                $repositoryDefinition->setArguments([
                    'mapper' => $mapperDefinition,
                ]);
            }
        } else {
            $repositoryDefinition->setFactory(get_class($this).'::createRepository')
                ->setArguments([
                    'entityClass' => $entityClass,
                    'mapper' => $mapperDefinition,
            ]);
        }

        return $repositoryDefinition;
    }

    /**
     * Override this method for extension
     *
     * @return void
     */
    protected function setupRepositories(): void
    {
        $mainConfig         = $this->getConfig();
        $entitiesMapTable   = $this->createEntitiesMapTable($mainConfig);
        // Same as does Nextras\Orm\Model\Model::getConfiguration(), but for our use
        $modelConfiguration = [
            [], [], [],
        ];
        $repositoryNamesMap = [];

        foreach ($entitiesMapTable as $entityName => $entityClass) {
            // Merge with main config
            $repositoryClass = self::replaceWildcard($mainConfig['repositoryClasses'], $entityName);
            $entityNameLower = lcfirst($entityName);
            $config          = [
                'entityName' => $entityName,
                'entityNameLower' => $entityNameLower,
                'entityClass' => $entityClass,
                'repositoryClass' => $repositoryClass,
                ] + $mainConfig;

            $repositoryServiceName = $this->builder->getByType($repositoryClass);
            if (!$repositoryServiceName) {
                // Repository NOT registered in DI - lets create default one...
                $repositoryServiceName = $this->extension->prefix('repository.'.$entityNameLower);
                $repositoryDefinition  = $this->createRepositoryDefinition($config);
                $this->builder->addDefinition($repositoryServiceName, $repositoryDefinition);
            } else {
                // Repository already registered in DI
                $repositoryDefinition = $this->builder->getDefinition($repositoryServiceName);
            }

            $repositoryNamesMap[$repositoryClass] = $repositoryServiceName;

            $modelConfiguration[0][$repositoryClass] = true;
            $modelConfiguration[1][$entityNameLower] = $repositoryClass;
            $modelConfiguration[2][$entityClass]     = $repositoryClass;

            $repositoryDefinition
                ->addSetup('setModel', [$this->extension->prefix('@model')])
                ->addSetup('if (isset(class_uses(?)[?])) { ?::setEntityClassNames(?->tags[\'entityClass\']); }',
                    ['@self', GenericRepositoryTrait::class, '@self', '@container'])
                ->addTag('repositoryClass', $repositoryClass)
                ->addTag('entityClass', $entityClass)
                ->addTag('nette.inject', true);
        }

        $this->builder->addDefinition($this->extension->prefix('repositoryLoader'))
            ->setClass(RepositoryLoader::class)
            ->setArguments([
                'repositoryNamesMap' => $repositoryNamesMap,
        ]);

        // Override services definitions from OrmExtension
        // Bit of a hack... or not? Hmmmmm... probably not :-)
        $this->builder->getDefinition($this->extension->prefix('model'))
                ->getFactory()->arguments['configuration'] = $modelConfiguration;

        $this->builder->getDefinition($this->extension->prefix('metadataStorage'))
                ->getFactory()->arguments['entityClassesMap'] = $modelConfiguration[2];
    }

    /**
     * Override setupRepositories for extension
     * @see setupRepositories
     * @see OrmExtension
     *
     * @return array|null
     */
    final public function loadConfiguration(): ?array
    {
        // Return array for launch of setup model and metadataStorage in extension
        return [];
    }

    /**
     * Override setupRepositories for extension
     * @see setupRepositories
     * @see OrmExtension
     *
     * @return array|null
     */
    final public function beforeCompile(): ?array
    {
        $this->setupRepositories();

        // Return null, everything is setup and going
        return null;
    }
}