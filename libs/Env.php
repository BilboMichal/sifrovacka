<?php
declare(strict_types=1);

namespace Libs;

use ErrorException;

/**
 * @todo $_ENV vs getenv() ?
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Env
{

    /**
     *
     * @param string $name
     * @param type $default
     * @param callable $convert
     * @param bool $require
     * @return type
     * @throws ErrorException
     */
    public static function get(string $name, $default = null, callable $convert = null, bool $require = false)
    {
        $value = $_ENV[$name] ?? null;
        if ($value === null) {
            if ($require) {
                throw new ErrorException(sprintf('ENV variable name "%s" is required', $name));
            }

            $value = $default;
        }

        if ($convert !== null) {
            $value = $convert($value);
        }

        return $value;
    }

    public static function require(string $name, callable $convert = null)
    {
        return self::get($name, null, $convert, true);
    }
}