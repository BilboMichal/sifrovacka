<?php

namespace Libs\Nette\Application;

use Nette\Utils\Arrays as NetteArrays;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Arrays extends NetteArrays
{

    public static function filterWhitelist(array $array, array $keys = []): array
    {
        return array_intersect_key($array, array_flip($keys));
    }

    public static function filterBlacklist(array $array, array $keys = []): array
    {
        return array_diff_key($array, array_flip($keys));
    }
}