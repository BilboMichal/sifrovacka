<?php
declare(strict_types=1);

namespace Libs\Nette\Application\Latte;

use Libs\Nette\Application\IdGenerator;
use InvalidArgumentException;
use Latte\Compiler;
use Latte\Engine;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class IdMacros extends MacroSet
{

    public static function install($obj)
    {
        if ($obj instanceof Compiler) {
            self::installOnCompiler($compiler);
        } elseif ($obj instanceof Engine) {
            self::installOnEngine($engine);
        } else {
            throw new InvalidArgumentException('Invalid $obj');
        }
    }

    public static function installOnCompiler(Compiler $compiler)
    {
        $me = new static($compiler);

        $idMacro        = 'echo %escape(\\'.IdGenerator::class.'::id($this->global->uiControl, ...[%node.args]));';
        $controlIdMacro = 'echo %escape(\\'.IdGenerator::class.'::controlId($this->global->uiControl, ...[%node.args]));';
        $formIdMacro    = 'echo %escape(\\'.IdGenerator::class.'::controlId(end($this->global->formsStack), ...[%node.args]));';
        $me->addMacro('id', $idMacro, null,
            function (MacroNode $node, PhpWriter $writer) use ($me, $idMacro): string {
            return $writer->write(' ?> id="<?php '.$idMacro.' ?>"<?php ');
        });
        $me->addMacro('controlId', $controlIdMacro);
        $me->addMacro('formId', $formIdMacro);
    }

    public static function installOnEngine(Engine $engine)
    {
        $engine->onCompile[] = function (Engine $engine) {
            self::installOnCompiler($engine->getCompiler());
        };
    }
}