<?php
declare(strict_types=1);

namespace Libs\Nette\Application\Latte;

use Nette\Application\UI\Control;
use Nette\Application\UI\Template;
use Nette\Bridges\ApplicationLatte\TemplateFactory as NetteTemplateFactory;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class TemplateFactory extends NetteTemplateFactory
{
    /** @var array */
    private $parameters = [];

    /**
     *
     * @param array $parameters
     * @return $this
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    public function createTemplate(Control $control = null, string $class = null): Template
    {
        $template = parent::createTemplate($control);
        $template->setParameters($this->parameters);

        return $template;
    }
}