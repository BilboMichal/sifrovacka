<?php
declare(strict_types=1);

namespace Libs\Nette\Application;

use Nette\Application\UI\Component;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Bridges\ApplicationLatte\TemplateFactory;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Controls\BaseControl;
use Nette\Forms\Form;
use Nette\InvalidArgumentException;
use Nette\Utils\Strings;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class IdGenerator
{

    public static function id(IContainer $rootComponent, $componentOrId = '', string $id = null): string
    {
        if (is_object($componentOrId)) {
            return self::controlId($rootComponent, $componentOrId, $id);
        } else {
            return self::controlId($rootComponent, '', $componentOrId);
        }
    }

    public static function controlId(IContainer $rootComponent, $component = '', string $id = null): string
    {
        $idSelector = '';
        $id         = (string) $id;
        if (Strings::startsWith($id, '#') || (is_string($component) && Strings::startsWith($component, '#'))) {
            $idSelector = '#';
        }
        $id = ltrim($id, '#');

        if (is_string($component)) {
            $relativeComponentId = ltrim($component, '#');
            $component           = $relativeComponentId === '' ?
                $rootComponent : $rootComponent->getComponent($relativeComponentId);
        }

        if ($component instanceof Component) {
            $componentId = $component->getUniqueId();
        } elseif ($component instanceof BaseControl) {
            $componentId = $component->getHtmlId();
        } elseif ($component instanceof Form) {
            $componentId = $component->getElementPrototype()->getAttribute('id');
        } else {
            throw new InvalidArgumentException(
                sprintf('$component is of type %s - it must be one of these types - string (name of control in component tree) or %s or %s or %s',
                    is_object($component) ? get_class($component) : gettype($component), Component::class,
                    BaseControl::class, Form::class)
            );
        }

        return $idSelector.trim($componentId.'_'.$id, '_');
    }

    public static function installOnFactory(TemplateFactory $templateFactory, string $filterNamePrefix = ''): TemplateFactory
    {
        $templateFactory->onCreate[] = function (Template $template) use ($filterNamePrefix) {
            if (isset($template->control)) {
                self::install($template, $template->control, $filterNamePrefix);
            }
        };

        return $templateFactory;
    }

    public static function install(object $filterObj, IContainer $rootComponent, string $filterNamePrefix = ''): object
    {
        if (!method_exists($filterObj, 'addFilter')) {
            throw new InvalidArgumentException('Given $filterObj object must have addFilter(string $name, callable $filter) method');
        }

        $filterObj->addFilter($filterNamePrefix.'id',
            function (...$filterArgs) use ($rootComponent): string {
            array_unshift($filterArgs, $rootComponent);
            return self::id(...$filterArgs);
        });
        $filterObj->addFilter($filterNamePrefix.'controlId',
            function (...$filterArgs) use ($rootComponent): string {
            array_unshift($filterArgs, $rootComponent);
            return self::controlId(...$filterArgs);
        });

        return $filterObj;
    }
}