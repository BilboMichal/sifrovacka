<?php
declare(strict_types=1);

namespace Libs\DI;

use Nette\DI\Container as NetteContainer;
use Psr\Container\ContainerInterface;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Container extends NetteContainer implements ContainerInterface
{

    public function get($id)
    {
        if ($service = $this->getByType($id, false)) {
            return $service;
        }

        return $this->getService($id);
    }

    public function has($id): bool
    {
        return $this->findAutowired($id) || $this->hasService($id);
    }
}