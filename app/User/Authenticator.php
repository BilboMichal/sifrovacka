<?php
declare(strict_types=1);

namespace App\User;

use App\Model\Repository\Repository;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Authenticator implements IAuthenticator
{
    /** @var array */
    private $adminCredentials;

    /** @var Repository */
    private $teamRepository;

    public function __construct(array $adminCredentials, Repository $teamRepository)
    {
        $this->adminCredentials = $adminCredentials;
        $this->teamRepository   = $teamRepository;
    }

    public function authenticate(array $credentials): IIdentity
    {
        list($username, $password) = $credentials;

        // This is admin...
        if ($this->adminCredentials['username'] === $username) {
            if ($this->adminCredentials['password'] === $password) {
                return new Identity('admin', ['admin'], ['username' => 'Admin']);
            } else {
                throw new AuthenticationException('Invalid admin access', self::INVALID_CREDENTIAL);
            }
        }

        $team = $this->teamRepository->findBy(['name' => $username])->fetch();
        if (!$team) {
            throw new AuthenticationException('Team not found', self::IDENTITY_NOT_FOUND);
        }

        if ($team->password !== $password) {
            throw new AuthenticationException('Team password wrong', self::INVALID_CREDENTIAL);
        }

        return new Identity('team_'.$team->id, ['team'], ['id' => $team->id, 'username' => $team->name]);
    }
}