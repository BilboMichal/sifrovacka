<?php
declare(strict_types=1);

namespace App\Datagrid;

use Nextras\Datagrid\Datagrid as NextrasDatagrid;
use Nextras\Orm\Collection\ICollection;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Datagrid extends NextrasDatagrid
{

    /**
     * Adds column
     * @param  string
     * @param  string
     * @return Column
     */
    public function addColumn($name, $label = null)
    {
        if (!$this->rowPrimaryKey) {
            $this->rowPrimaryKey = $name;
        }

        $label = $label ? $this->translate($label) : ucfirst($name);
        return $this->columns[$name] = new Column($name, $label, $this);
    }

    /**
     *
     * @return Column[]
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    public function setCollectionDataSource(ICollection $collection)
    {
        return $this->setDataSourceCallback(CollectionDataSource::create($this, $collection));
    }

    public function setPagination($itemsPerPage, callable $itemsCountCallback = null)
    {
        if ($itemsCountCallback === null) {
            $itemsCountCallback = function () {
                
            };
        }

        return parent::setPagination($itemsPerPage, $itemsCountCallback);
    }

    public function handleSort()
    {
        if (!in_array($this->orderType, [self::ORDER_ASC, self::ORDER_DESC], true)) {
            $this->orderType = self::ORDER_ASC;
        }
        parent::handleSort();
    }
}