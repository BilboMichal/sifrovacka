<?php
declare(strict_types=1);

namespace App\Datagrid;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class DatagridFactory
{

    public function create(): Datagrid
    {
        $datagrid = new Datagrid;

        return $datagrid;
    }
}