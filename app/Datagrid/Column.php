<?php
declare(strict_types=1);

namespace App\Datagrid;

use App\Helpers\OptionsTrait;
use Nextras\Datagrid\Column as NextrasColumn;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Column extends NextrasColumn
{

    use OptionsTrait;

    public function getNewState()
    {
        if ($this->isAsc()) {
            return Datagrid::ORDER_DESC;
        } else {
            return Datagrid::ORDER_ASC;
        }
    }
}