<?php
declare(strict_types=1);

namespace App\Datagrid;

use Nette\Utils\Paginator;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Mapper\Dbal\CustomFunctions\IQueryBuilderFilterFunction;

/**
 * Source for datagrid using Dbal collection
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class CollectionDataSource
{
    /** @var Datagrid */
    private $datagrid;

    /** @var ICollection */
    private $collection;

    /** @var int|null */
    private $limit;

    /**
     *
     * @param Datagrid $datagrid
     * @param ICollection $collection
     * @param int|null $limit
     */
    public function __construct(Datagrid $datagrid, ICollection $collection, int $limit = null)
    {
        $this->datagrid = $datagrid;
        $this->collection = $collection;
        $this->limit = $limit;
    }

    /**
     *
     * @param Datagrid $datagrid
     * @param ICollection $collection
     * @param int|null $limit
     * @return self
     */
    public static function create(Datagrid $datagrid, ICollection $collection, int $limit = null): self
    {
        return new self(...func_get_args());
    }

    /**
     *
     * @param array $filter
     * @param array $order
     * @param Paginator $paginator
     * @return ICollection
     */
    public function __invoke(array $filter = [], array $order = null, Paginator $paginator = null): ICollection
    {
        $collection = $this->collection;

        if ($order !== null) {
            list($name, $direction) = $order;
            $column = $this->datagrid->getColumn($name);
            $orderName = $column->getOption('orderName');
            $collection = $collection->orderBy($orderName ?? $name, $direction);
        }

        if (isset($this->datagrid['form']['filter'])) {
            foreach ($this->datagrid['form']['filter']->getValueControls() as $name => $filterControl) {
                if (isset($filter[$name])) {
                    $filterFunction = $filterControl->getOption('filter');
                    $filterName = $filterControl->getOption('filterName');
                    $filterName = $filterName ?? $name;
                    $filterValue = $filter[$name];
                    if (isset($filterFunction)) {
                        if (is_callable($filterFunction) && !$filterFunction instanceof IQueryBuilderFilterFunction) {
                            $collection = call_user_func($filterFunction, $collection, $filterName, $filterValue);
                        } else {
                            $collection = $collection->findBy([$filterFunction, $filterName, $filterValue]);
                        }
                    } else {
                        $collection = $collection->findBy([$filterName => $filterValue]);
                    }
                }
            }
        }

        if ($paginator !== null) {
            $paginator->setItemCount($collection->countStored());

            $collection = $collection->limitBy($paginator->getItemsPerPage(),
                $paginator !== null ? $paginator->getOffset() : null);
        }

        return $collection;
    }
}