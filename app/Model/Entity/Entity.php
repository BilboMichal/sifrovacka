<?php
declare(strict_types=1);

namespace App\Model\Entity;

use JsonSerializable;
use Nette\Iterators\Mapper;
use Nextras\Orm\Entity\Entity as EntityParent;
use Nextras\Orm\Entity\ToArrayConverter;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
abstract class Entity extends EntityParent implements JsonSerializable
{

    /**
     * alias toArray
     *
     * @return array
     */
    final public function getScalarData(): array
    {
        return $this->toArray(ToArrayConverter::RELATIONSHIP_AS_ID);
    }

    /**
     * alias toArray
     *
     * @return array
     */
    final public function getData(): array
    {
        return $this->toArray();
    }

    /**
     * Adds entity data
     *
     * @param array $data
     * @return $this
     */
    final public function addData(array $data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $this->$key->setData($value);
            } else {
                $this->$key = $value;
            }
        }

        return $this;
    }

    /**
     * App\Model\Entity\MyEntity --> myEntity
     *
     * @return string
     */
    final public function getEntityName(): string
    {
        $className = $this->getMetadata()->className;
        return lcfirst(substr($className, strrpos($className, '\\') + 1));
    }

    /**
     * property --> myEntityProperty
     *
     * @param string $name
     * @return string
     */
    final public function getPrefixedName(string $name): string
    {
        return $this->getEntityName().ucfirst($name);
    }

    protected static function entitiesIds(iterable $entities): array
    {
        return array_map(function (Entity $entity) {
            return $entity->id;
        }, is_array($entities) ? $entities : iterator_to_array($entities));
    }

    public function __toString()
    {
        return isset($this->id) ? (string) $this->id : '';
    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return $this->getScalarData();
    }
}