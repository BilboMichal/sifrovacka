<?php
declare(strict_types=1);

namespace App\Model\Entity;

/**
 * @property    int             $id         {primary}
 * @property    string          $name
 * @property    string          $password
 * @property    Progress[]      $progress   {1:m Progress::$team}
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Team extends Entity
{
    
}