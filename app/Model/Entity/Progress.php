<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Nextras\Dbal\Utils\DateTimeImmutable;

/**
 * @property    int                 $id          {primary}
 * @property    Team|null           $team        {m:1 Team::$progress}
 * @property    Cipher|null         $cipher      {m:1 Cipher::$progress}
 * @property    string              $event       {enum self::EVENT_*}
 * @property    DateTimeImmutable   $time
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Progress extends Entity
{
    const EVENT_ARRIVED = 'ARRIVED';
    const EVENT_HELP    = 'HELP';
    const EVENT_SOLVED  = 'SOLVED';
    const EVENT_SKIPPED = 'SKIPPED';

}