<?php
declare(strict_types=1);

namespace App\Model\Entity;

/**
 * @property        int        $id      {primary}
 * @property        string     $name
 * @property        string     $place
 * @property        string     $password
 * @property        string     $task
 * @property        string     $help
 * @property        string     $answer
 * @property        int        $sequence
 *
 * @property        Progress[]    $progress   {1:m Progress::$cipher}
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Cipher extends Entity
{

    public function getTeamProgress(int $team): object
    {
        $teamProgressItems = $this->progress->get()->findBy(['team' => $team]);
        $teamProgress      = [];
        foreach ($teamProgressItems as $teamProgressItem) {
            $teamProgress[strtolower($teamProgressItem->event)] = $teamProgressItem->time;
        }

        return (object) $teamProgress;
    }
}