<?php

namespace App\Model;

use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Model\Model as ParentOrm;
use App\Model\Repository\Repository;

/**
 * @property-read Repository $team
 * @property-read Repository $teamMember
 * @property-read Repository $cipher
 * @property-read Repository $progress
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Orm extends ParentOrm
{

    public function createEntity(string $entityClass): IEntity
    {
        return $this->getRepositoryForEntity($entityClass)->create();
    }
}