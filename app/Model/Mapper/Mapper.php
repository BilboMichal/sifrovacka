<?php
declare(strict_types=1);

namespace App\Model\Mapper;

use Libs\Nextras\Orm\GenericRepositoryTrait;
use Libs\Nextras\Orm\SimpleDbalMapperTrait;
use Nextras\Orm\Mapper\Dbal\DbalMapper;
use Nextras\Orm\Mapper\Dbal\StorageReflection\UnderscoredStorageReflection;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Mapper extends DbalMapper
{

    use GenericRepositoryTrait;
    use SimpleDbalMapperTrait;

    public function createStorageReflection(): UnderscoredStorageReflection
    {
        $reflection                                = parent::createStorageReflection();
        $reflection->manyHasManyStorageNamePattern = '%s2%s';

        return $reflection;
    }
}