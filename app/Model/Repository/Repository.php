<?php
declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Entity;
use App\Model\Repository\Filters\Filter;
use Libs\Nextras\Orm\GenericRepositoryTrait;
use Libs\Nextras\Orm\SimpleRepositoryTrait;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Repository\Repository as RepositoryParent;

/**
 * @method ICollection|Entity[] findName()
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Repository extends RepositoryParent
{

    use SimpleRepositoryTrait;
    use GenericRepositoryTrait;

    /**
     * App\Model\Entity\MyEntity --> myEntity
     *
     * @return string
     */
    final public function getEntityName(): string
    {
        $className = $this->getEntityMetadata()->className;
        return lcfirst(substr($className, strrpos($className, '\\') + 1));
    }

    /**
     * property --> myEntityProperty
     *
     * @param string $name
     * @return string
     */
    final public function getPrefixedName(string $name): string
    {
        return $this->getEntityName().ucfirst($name);
    }

    public function create(array $data = []): Entity
    {
        $entityClassName = $this->getEntityClassName($data);
        $entity = new $entityClassName;
        $this->attach($entity);
        $entity->addData($data);

        return $entity;
    }

    /**
     *
     * @param int|string $id
     * @return Entity|null
     */
    public function find($id): ?Entity
    {
        return $this->findById($id)->fetch();
    }

    public function findByFilter(callable $filter)
    {
        
    }
}