<?php

namespace App\Model\DI;

use App\Model\Entity\SubbrainRelation;
use App\Model\Mapper\Mapper;
use App\Model\Repository\Repository;
use App\Model\Repository\SubbrainRelationRepository;
use Libs\Nextras\Orm\DI\RepositoryFinder as RepositoryFinderParent;
use League\Flysystem\MountManager;
use Nette\Caching\Cache;
use Nette\DI\Container;
use Nextras\Dbal\IConnection;
use Nextras\Orm\Mapper\Dbal\DbalMapperCoordinator;
use Nextras\Orm\Mapper\IMapper;
use Nextras\Orm\Repository\IDependencyProvider;
use Nextras\Orm\Repository\IRepository;
use Nextras\Orm\StorageReflection\StringHelper;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class RepositoryFinder extends RepositoryFinderParent
{

    public static function createRepository(string $entityClass, IMapper $mapper, Container $container): IRepository
    {
        $entityClassParents = class_parents($entityClass);
        $repositoryClass    = Repository::class;

        $repository = (new $repositoryClass($mapper, $container->getByType(IDependencyProvider::class, false)))
            ->setEntityClassName($entityClass);

        return $repository;
    }

    public static function createMapper(string $name, Cache $cache, Container $container): IMapper
    {
        $mapper = (new Mapper($container->getByType(IConnection::class),
                $container->getByType(DbalMapperCoordinator::class), $cache));

        // Mapování názvů entit na DB tabulky
        $dbTables = $container->parameters['repositoryFinder']['dbTables'];
        $mapper->setTableName($dbTables[$name]);

        return $mapper;
    }
}