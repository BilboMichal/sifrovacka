<?php
declare(strict_types=1);

namespace App;

use Libs\Env;
use Dotenv\Dotenv;
use Tester\Environment;

class Bootstrap
{
    public static function boot(): Configurator
    {        
        $appDir  = __DIR__;
        $libsDir = __DIR__.'/../libs';

        require_once $appDir.'/Configurator.php';
        Dotenv::createUnsafeImmutable($appDir.'/..')->load();

        $configurator = new Configurator;
        $configurator->setTempDirectory($appDir.'/../temp')
            ->setTimeZone('Europe/Prague');

        $configurator->createRobotLoader()
            ->addDirectory($appDir)
            ->addDirectory($libsDir)
            ->register();

        $debug = Env::get('DEBUG', false, 'boolval');
        $configurator->setDebugMode($debug);
        $configurator->enableTracy($appDir.'/../log');

        // Ignore deprecated from now on...
        error_reporting(E_ALL ^ E_DEPRECATED ^ E_USER_DEPRECATED);

        $configDir = $appDir.'/config';
        $configurator
            ->addParameters([
                'debug' => $debug,
                'catchExceptions' => Env::get('CATCH_EXCEPTIONS', !$debug, 'boolval'),
                'assetsMinified' => Env::get('CATCH_EXCEPTIONS', !$debug, 'boolval'),
            ])->addConfig($configDir.'/config.neon')
            ->addConfig($configDir.'/project.neon')
            ->addConfigIfExists($configDir.'/local.neon');

        $configurator->addMonitorDir($appDir.'/Model/Entity');
        $configurator->addMonitorDir($appDir.'/Model/Repository');

        return $configurator;
    }

    public static function bootForTests(): Configurator
    {
        $configurator = self::boot();
        Environment::setup();
        return $configurator;
    }
}