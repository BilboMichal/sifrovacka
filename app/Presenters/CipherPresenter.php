<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Datagrid\Datagrid;
use App\Forms\Form;
use App\Forms\Hydrator;
use App\Model\Entity\Cipher;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;
use Nette\Forms\Container;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class CipherPresenter extends BasePresenter
{

    use DatagridTrait;
    //
    /** @var Cipher */
    private $cipher;

    protected function loginCheck()
    {
        parent::loginCheck();
        if (!$this->user->isInRole('admin')) {
            throw new ForbiddenRequestException;
        }
    }

    public function actionList()
    {
        $this->template->title = 'Šifry';
    }

    public function handleRemove(int $id)
    {
        $cipher = $this->orm->cipher->find($id);
        if ($cipher !== null) {
            $this->orm->cipher->removeAndFlush($cipher);
            $this->flashMessage('Šifra smazána');
        }

        $this->redirect('this');
    }

    public function actionEdit(int $id = null)
    {
        if ($id !== null) {
            $cipher = $this->orm->cipher->find($id);
            if ($cipher === null) {
                throw new BadRequestException;
            }

            $this->cipher = $cipher;
        } else {
            $this->cipher = $this->orm->cipher->create();
        }
    }

    protected function createComponentDatagrid(): Datagrid
    {
        $datagrid = $this->datagridFactory->create();
        $datagrid->addColumn('id');
        $datagrid->addColumn('name', 'Název šifry');
        $datagrid->addColumn('place', 'Místo, kde se nachází šifra');
        $datagrid->addColumn('password', 'Vstupní heslo pro zobrazení šifry');
        $datagrid->addColumn('task', 'Zadání šifry');
        $datagrid->addColumn('help', 'Nápověda');
        $datagrid->addColumn('answer', 'Odpověď na šifru');

        $collection = $this->orm->cipher->findAll()
            ->orderBy('sequence');
        $datagrid->setCollectionDataSource($collection);
        $datagrid->setPagination(1000);

        $this->setupDatagridTemplates($datagrid);

        return $datagrid;
    }

    protected function createComponentForm(): Form
    {
        $form = $this->formFactory->create();
        $form->addText('name', 'Název šifry')
            ->setRequired();
        $form->addText('sequence', 'Pořadí šifry')
            ->setHtmlType('number')
            ->setHtmlAttribute('min', 1)
            ->setRequired();
        $form->addText('place', 'Místo')
            ->setRequired();
        $form->addText('password_cipher', 'Heslo')
            ->setOption('hydrate', 'password')
            ->setRequired();
        $form->addTextArea('task', 'Zadání')
            ->setRequired();
        $form->addTextArea('help', 'Nápověda');
        $form->addText('answer', 'Odpověď')
            ->setRequired();
        $form->addSubmit('save', 'Uložit');
        $form->addSubmit('saveReturn', 'Uložit a vrátit se');

        Hydrator::hydrateIn($form, $this->cipher);

        $form->onSuccess[] = function (Form $form) {
            $values = $form->getValues();
            Hydrator::hydrateOut($form, $this->cipher);

            $this->orm->cipher->persistAndFlush($this->cipher);

            $this->flashMessage('Šifra uložena');
            if ($form['saveReturn']->isSubmittedBy()) {
                $this->redirect('list');
            } else {
                $this->redirect('this', $this->cipher->id);
            }
        };

        return $form;
    }
}