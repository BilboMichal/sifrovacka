<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\Form;
use App\Model\Entity\Cipher;
use App\Model\Entity\Progress;
use Nette\Application\ForbiddenRequestException;
use Nette\Utils\Strings;
use Nextras\Dbal\UniqueConstraintViolationException;
use Nextras\Dbal\Utils\DateTimeImmutable;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class GamePresenter extends BasePresenter
{
    const HELP_OFFSET = '+ 15 minutes';

    /** @var Cipher @internal */
    private $cipher;

    /** @var object @internal */
    private $teamProgress;

    /** @var DateTimeImmutable @internal */
    private $now;

    protected function loginCheck()
    {
        parent::loginCheck();
        if (!$this->user->isInRole('team')) {
            throw new ForbiddenRequestException;
        }
    }

    public function renderList()
    {
        $this->template->title = 'Seznam úkolů';
    }

    public function actionTask(int $id)
    {
        $this->cipher = $cipher       = $this->orm->cipher->find($id);
        if (!$cipher) {
            $this->flashMessage('Nesprávný úkol', self::FLASH_ERROR);
            $this->redirect('list');
        }
        $this->teamProgress = $cipher->getTeamProgress($this->getTeamId());
        $this->now          = new DateTimeImmutable;
    }

    public function renderTask(int $id)
    {
        $this->template->title                = sprintf('Úkol č. %d - %s', $this->cipher->sequence, $this->cipher->name);
        $this->template->teamProgress         = $this->teamProgress;
        $this->template->cipher               = $this->cipher;
        $this->template->previous             = $this->orm->cipher->findBy(['sequence' => $this->cipher->sequence - 1])->fetch();
        $this->template->teamProgressPrevious = $this->template->previous !== null ? $this->template->previous->getTeamProgress($this->getTeamId())
                : null;
        $this->template->next                 = $this->orm->cipher->findBy(['sequence' => $this->cipher->sequence + 1])->fetch();
        $this->template->now                  = new DateTimeImmutable;
        $this->template->helpOffset           = self::HELP_OFFSET;
    }

    public function handleSkip()
    {
        if (!isset($this->teamProgress->solved) && !isset($this->teamProgress->skipped)) {
            $this->saveProgress(Progress::EVENT_SKIPPED);
            $this->flashMessage('Úkol přeskočen...', self::FLASH_SUCCESS);
            $this->redirect('this');
        }
    }

    public function handleHelp()
    {
        if ($this->now >= $this->teamProgress->arrived->modify(self::HELP_OFFSET)) {
            $this->saveProgress(Progress::EVENT_HELP);
            $this->flashMessage('Nápověda odemčena...', self::FLASH_SUCCESS);
            $this->redirect('this');
        }
    }

    // ----------------------------------------------------------------------------------------
    // internal

    protected function saveProgress(string $event)
    {
        $progress = $this->orm->progress->create([
            'team' => $this->getTeamId(),
            'cipher' => $this->cipher,
            'event' => strtoupper($event),
            'time' => new DateTimeImmutable,
        ]);
        try {
            $this->orm->progress->persistAndFlush($progress);
        } catch (UniqueConstraintViolationException $ex) {
            
        }
    }

    protected function canUseHelp(): bool
    {
        $now = new DateTimeImmutable;
        if (isset($this->teamProgress->arrived) && !isset($this->teamProgress->helped)) {
            bdump($now->diff($this->teamProgress->arrived));
        }
    }

    // ----------------------------------------------------------------------------------------
    // forms


    protected function createComponentArriveForm(): Form
    {
        $form = $this->formFactory->create();
        $form->addText('arrive_password',
                'Zadejte odemykací heslo k příchodu k úkolu (akceptují se malá i velká písmena)')
            ->setRequired();
        $form->addSubmit('send', 'Odeslat');

        $form->onSuccess[] = function (Form $form) {
            $values = $form->getValues();
            if (strtolower($values->arrive_password) === strtolower($this->cipher->password)) {
                $this->flashMessage('Správné heslo, uložen příchod a úkol odemčen...', self::FLASH_SUCCESS);
                $this->saveProgress(Progress::EVENT_ARRIVED);
                $this->redirect('this');
            } else {
                $this->flashMessage('Nesprávné heslo, dostanete ho na papírku při příchodu k úkolu...',
                    self::FLASH_ERROR);
            }
        };

        return $form;
    }

    protected function createComponentAnswerForm(): Form
    {
        $form = $this->formFactory->create();
        $form->addText('answer',
                'Zadejte odpověď na otázku úkolu (akceptují se malá i velká písmena, ignoruje se diakritika)')
            ->setRequired();
        $form->addSubmit('send', 'Odeslat');

        $form->onSuccess[] = function (Form $form) {
            $values = $form->getValues();
            if (Strings::lower(Strings::toAscii($values->answer)) === Strings::lower(Strings::toAscii($this->cipher->answer))) {
                $this->flashMessage('Správná odpověď!', self::FLASH_SUCCESS);
                $this->saveProgress(Progress::EVENT_SOLVED);
                $this->redirect('this');
            } else {
                $this->flashMessage('Nesprávná odpověď...', self::FLASH_ERROR);
            }
        };

        return $form;
    }
}