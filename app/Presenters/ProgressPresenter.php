<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Model\Entity\Progress;
use Nextras\Datagrid\Datagrid;

/**
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class ProgressPresenter extends BasePresenter
{

    use DatagridTrait;

    protected function loginCheck()
    {
        parent::loginCheck();
        if (!$this->user->isInRole('admin')) {
            throw new ForbiddenRequestException;
        }
    }

    public function actionList()
    {
        $this->template->title = 'Průběh řešení';
    }

    public function handleRemove(int $id)
    {
        $progress = $this->orm->progress->find($id);
        if ($progress !== null) {
            $this->orm->progress->removeAndFlush($progress);
            $this->flashMessage('Smazáno');
        }

        $this->redirect('this');
    }

    protected function createComponentDatagrid(): Datagrid
    {
        $datagrid = $this->datagridFactory->create();
        $datagrid->addColumn('id');
        $datagrid->addColumn('team', 'Tým')
            ->enableSort();
        $datagrid->addColumn('cipher', 'Šifra')
            ->enableSort();
        $datagrid->addColumn('event', 'Událost')
            ->enableSort();
        $datagrid->addColumn('time', 'Datum a čas události')
            ->enableSort();

        $collection = $this->orm->progress->findAll();
        $datagrid->setCollectionDataSource($collection);
        $datagrid->setPagination(1000);

        $datagrid->onRender[] = function (Datagrid $datagrid) {
            $datagrid->getTemplate()->eventTitles = [
                Progress::EVENT_ARRIVED => 'Příchod',
                Progress::EVENT_HELP => 'Nápověda',
                Progress::EVENT_SOLVED => 'Vyřešeno',
                Progress::EVENT_SKIPPED => 'Přeskočeno',
            ];
        };

        $this->setupDatagridTemplates($datagrid);

        return $datagrid;
    }
}