<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Datagrid\Datagrid;
use App\Forms\Form;
use App\Forms\Hydrator;
use App\Model\Entity\Team;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class TeamPresenter extends BasePresenter
{

    use DatagridTrait;
    //
    /** @var Team */
    private $team;

    protected function loginCheck()
    {
        parent::loginCheck();
        if (!$this->user->isInRole('admin')) {
            throw new ForbiddenRequestException;
        }
    }

    public function actionList()
    {
        $this->template->title = 'Týmy';
    }

    public function handleRemove(int $id)
    {
        $team = $this->orm->team->find($id);
        if ($team !== null) {
            $this->orm->team->removeAndFlush($team, false);
            $this->flashMessage('Tým smazán');
        }

        $this->redirect('this');
    }

    public function actionEdit(int $id = null)
    {
        if ($id !== null) {
            $team = $this->orm->team->find($id);
            if ($team === null) {
                throw new BadRequestException;
            }

            $this->team = $team;
        } else {
            $this->team = $this->orm->team->create();
        }
    }

    protected function createComponentDatagrid(): Datagrid
    {
        $datagrid = $this->datagridFactory->create();
        $datagrid->addColumn('id');
        $datagrid->addColumn('name', 'Název týmu');
        $datagrid->addColumn('password', 'Heslo týmu');

        $collection = $this->orm->team->findAll();
        $datagrid->setCollectionDataSource($collection);
        $datagrid->setPagination(1000);

        $this->setupDatagridTemplates($datagrid);

        return $datagrid;
    }

    protected function createComponentForm(): Form
    {
        $form = $this->formFactory->create();
        $form->addText('name', 'Název týmu')
            ->setRequired();
        $form->addText('password_team', 'Heslo týmu')
            ->setOption('hydrate', 'password')
            ->setRequired();
        $form->addSubmit('save', 'Uložit');
        $form->addSubmit('saveReturn', 'Uložit a vrátit se');

        Hydrator::hydrateIn($form, $this->team);

        $form->onSuccess[] = function (Form $form) {
            $values = $form->getValues();
            Hydrator::hydrateOut($form, $this->team);

            $this->orm->team->persistAndFlush($this->team);

            $this->flashMessage('Tým uložen');
            if ($form['saveReturn']->isSubmittedBy()) {
                $this->redirect('list');
            } else {
                $this->redirect('this', $this->team->id);
            }
        };

        return $form;
    }
}