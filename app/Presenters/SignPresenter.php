<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class SignPresenter extends BasePresenter
{

    protected function loginCheck()
    {
        
    }

    protected function createComponentLoginForm(): Form
    {
        $form = $this->formFactory->create();
        $form->addText('username', 'Přihlašovací jméno');
        $form->addPassword('password', 'Heslo');

        $form->addSubmit('login', 'Přihlásit se');

        $form->onSuccess[] = function (Form $form) {
            $values = $form->getValues();
            try {
                $this->user->login($values->username, $values->password);
                $this->redirect('Home:');
            } catch (AuthenticationException $ex) {
                if ($ex->getCode() === IAuthenticator::IDENTITY_NOT_FOUND) {
                    $this->flashMessage('Nesprávný tým', self::FLASH_ERROR);
                } elseif ($ex->getCode() === IAuthenticator::INVALID_CREDENTIAL) {
                    $this->flashMessage('Nesprávné heslo', self::FLASH_ERROR);
                }
            }
        };

        return $form;
    }
}