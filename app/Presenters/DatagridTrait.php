<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Datagrid\CollectionDataSource;
use App\Datagrid\DatagridFactory;
use Nextras\Datagrid\Datagrid;
use Nextras\Orm\Collection\ICollection;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
trait DatagridTrait
{
    /** @var DatagridFactory */
    protected $datagridFactory;

    abstract public function getTemplateDir(): string;

    abstract public function getTemplatesDir(): string;

    public function injectDatagridFactory(DatagridFactory $datagridFactory = null)
    {
        $this->datagridFactory = $datagridFactory;

        return $this;
    }

    final protected function setupDatagridTemplates(Datagrid $datagrid): self
    {
        $datagridTemplateFileName = '_grid.latte';
        $datagridTemplates        = [
            $this->getTemplateDir().'/'.$datagridTemplateFileName,
            $this->getTemplatesDir().'/'.$datagridTemplateFileName,
        ];

        foreach ($datagridTemplates as $datagridTemplate) {
            if (file_exists($datagridTemplate)) {
                $datagrid->addCellsTemplate($datagridTemplate);
            }
        }

        return $this;
    }
}