<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\FormFactory;
use App\Model\Entity\Team;
use App\Model\Orm;
use Nette\Application\Helpers;
use Nette\Application\IResponse;
use Nette\Application\UI\Presenter as NettePresenter;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends NettePresenter
{
    //
    const FLASH_INFO    = 'info';
    const FLASH_SUCCESS = 'success';
    const FLASH_WARNING = 'warning';
    const FLASH_ERROR   = 'error';

    /** @var Orm */
    protected $orm;

    /** @var FormFactory */
    protected $formFactory;

    /** @var object */
    protected $project;

    // --------------------------------------------------------------------
    // get/set

    /**
     *
     * @param Orm $orm
     * @param FormFactory $formFactory
     */
    final public function injectBaseServices(Orm $orm, FormFactory $formFactory)
    {
        $this->orm         = $orm;
        $this->formFactory = $formFactory;
    }

    final public function setProject(array $project)
    {
        $this->project = (object) $project;
        return $this;
    }

    final public function getUsername(): string
    {
        $identityData = $this->user->getIdentity()->getData();
        return $identityData['username'];
    }

    final public function getTeamId(): ?int
    {
        if ($this->user->isLoggedIn()) {
            $identityData = $this->user->getIdentity()->getData();
            return $identityData['id'] ?? null;
        } else {
            return null;
        }
    }

    /**
     * Presenter name without modules
     *
     * @return string
     */
    final public function getShortName(): string
    {
        [, $shortName] = Helpers::splitName($this->getName());
        return $shortName;
    }

    /**
     * Presenter global templates directory
     *
     * @return string
     */
    public function getTemplatesDir(): string
    {
        return dirname(static::getReflection()->getFileName()).'/templates';
    }

    /**
     * Presenter single template directory
     *
     * @return string
     */
    public function getTemplateDir(): string
    {
        return $this->getTemplatesDir().'/'.$this->getShortName();
    }

    // --------------------------------------------------------------------
    // runtime

    protected function startup(): void
    {
        parent::startup();
        $this->loginCheck();
    }

    protected function loginCheck()
    {
        if (!$this->user->isLoggedIn()) {
            $this->flashMessage('Pro přístup do systému se musíte přihlásit', self::FLASH_WARNING);
            $this->redirect('Sign:login');
        }
    }

    public function redirectUrl(string $url, int $httpCode = null): void
    {
        if ($this->isAjax()) {
            $this->payload->path = $url;
            $this->sendPayload();
        }

        parent::redirectUrl($url, $httpCode);
    }

    public function formatTemplateFiles(): array
    {
        $templateFiles   = parent::formatTemplateFiles();
        $templatesDir    = $this->getTemplatesDir();
        $templateFiles[] = $templatesDir.'/'.$this->view.'.latte';

        return $templateFiles;
    }

    public function beforeRender(): void
    {
        parent::beforeRender();

        if ($this->user->isLoggedIn()) {
            $this->template->username = $this->getUsername();
        }
        $this->template->project      = $this->project;
        $this->template->nmodulesPath = $this->template->basePath.'/node_modules';
        $this->template->libsPath     = $this->template->basePath.'/libs';
        $this->template->templatesDir = $this->getTemplatesDir();
        $this->template->templateDir  = $this->getTemplateDir();

        $this->template->teamId = $this->getTeamId();

        $this->template->ciphersNav = $this->orm->cipher->findAll()->orderBy('sequence');

        $this->template->adminMenuItems = [
            (object) ['target' => 'Team:list', 'title' => 'Týmy', 'icon' => 'fas fa-user-friends'],
            (object) ['target' => 'Cipher:list', 'title' => 'Šifry', 'icon' => 'fas fa-tasks'],
            (object) ['target' => 'Progress:list', 'title' => 'Průběh řešení', 'icon' => 'fas fa-chart-line'],
        ];
    }

    public function afterRender(): void
    {
        parent::afterRender();

        if ($this->isAjax() && isset($this->template->title)) {
            $this->payload->title = $this->template->title;
        }
    }

    public function shutdown(IResponse $response): void
    {
        parent::shutdown($response);

        if ($this->isAjax() && isset($this->payload->state)) {
            $this->payload->path = $this->link('this', array_filter($this->payload->state));
        }
    }

    public function handleLogout()
    {
        $this->user->logout();
        $this->redirect('this');
    }
}