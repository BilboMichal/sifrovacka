<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\Form;
use App\Forms\FormFactory;
use Nette\Forms\ISubmitterControl;
use Nextras\Dbal\ConstraintViolationException;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
trait FormTrait
{
    /** @var FormFactory */
    protected $formFactory;

    /**
     *
     * @param FormFactory $formFactory
     * @return $this
     */
    public function injectFormFactory(FormFactory $formFactory = null)
    {
        $this->formFactory = $formFactory;
        return $this;
    }

    /**
     *
     * @param Form $form
     * @return bool
     */
    public function formSave(Form $form): bool
    {
        $submit = $form->isSubmitted();

        if ($submit instanceof ISubmitterControl) {
            $beforeSave = $submit->getOption('beforeSave');
            if ($beforeSave !== null) {
                call_user_func($beforeSave, $submit, $form);
            }
        }

        try {
            $this->repository->persist($this->entity);
        } catch (ConstraintViolationException $ex) {
            $this->flashMessage($this->messageConstraintViolation($ex), self::FLASH_ERROR);
            return false;
        }

        $this->repository->flush();
        $this->flashMessage($this->messageSaveSuccess($this->entity), self::FLASH_SUCCESS);

        if ($submit instanceof ISubmitterControl) {
            $afterSave = $submit->getOption('afterSave');
            if ($afterSave !== null) {
                call_user_func($afterSave, $submit, $form);
            }
        }

        return true;
    }

    /**
     *
     * @param Form $form
     * @return $this
     */
    final protected function setupFormButtons(Form $form): self
    {
        $form->addSubmit('save', 'Save');
        $form->addSubmit('saveReturn', 'Save and return')
            ->setOption('afterSave', function () {
                $this->redirect('list');
            });

        return $this;
    }
}