<?php
declare(strict_types=1);

namespace App\Presenters;

use Nette\Application\BadRequestException;
use Nette\Application\IResponse;
use Nette\Application\Request;
use Nette\Application\Responses\CallbackResponse;
use Nette\Application\Responses\TextResponse;
use Nette\Http\IRequest as IHttpRequest;
use Nette\Http\IResponse as IHttpResponse;
use Tracy\ILogger;

final class ErrorPresenter extends BasePresenter
{
    /** @var ILogger */
    private $logger;

    public function __construct(ILogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return IResponse
     */
    public function run(Request $request): IResponse
    {
        $e = $request->getParameter('exception');

        if ($e instanceof BadRequestException) {
            return new TextResponse('Error '.$e->getCode());
        } else {
            $this->logger->log($e, ILogger::EXCEPTION);
            return new CallbackResponse(function (IHttpRequest $httpRequest, IHttpResponse $httpResponse): void {
                if (preg_match('#^text/html(?:;|$)#', (string) $httpResponse->getHeader('Content-Type'))) {
                    require __DIR__.'/templates/Error/500.phtml';
                }
            });
        }
    }
}