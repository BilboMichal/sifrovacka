<?php
declare(strict_types=1);

namespace App\Helpers;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
trait OptionsTrait
{
    // Copied from Nette...
    //
    /** @var array */
    private $options = [];

    /**
     * Sets user-specific option.
     * @return static
     */
    public function setOption($key, $value)
    {
        if ($value === null) {
            unset($this->options[$key]);
        } else {
            $this->options[$key] = $value;
        }
        return $this;
    }

    /**
     * Returns user-specific option.
     * @return mixed
     */
    public function getOption($key, $default = null)
    {
        return $this->options[$key] ?? $default;
    }

    /**
     * Returns user-specific options.
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}