<?php

namespace App;

use DirectoryIterator;
use Nette\Configurator as NetteConfigurator;
use Nette\DI\Config\Loader;
use Tracy\Debugger;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Configurator extends NetteConfigurator
{
    private $monitorDirs = [];

    public function addConfigIfExists($config)
    {
        if (file_exists($config)) {
            $this->addConfig($config);
        }

        return $this;
    }

    public function createLoader(): Loader
    {
        $loader = new class() extends Loader {
            /** @var array */
            public $monitorDirs = [];

            public function getDependencies(): array
            {
                $dependencies = parent::getDependencies();
                foreach ($this->monitorDirs as $monitorDir) {
                    foreach (new DirectoryIterator($monitorDir) as $monitorDirFile) {
                        if ($monitorDirFile->isFile() && $monitorDirFile->getExtension() === 'php') {
                            $dependencies[] = $monitorDir.'/'.$monitorDirFile;
                        }
                    }
                    $dependencies[] = $monitorDir;
                }
                return $dependencies;
            }
        };
        $loader->monitorDirs = $this->monitorDirs;

        return $loader;
    }

    public function enableTracy(string $logDir = null, string $email = null): void
    {
        parent::enableTracy($logDir, $email);
        Debugger::$showLocation = true;
        Debugger::$strictMode   = true;
        Debugger::$maxLength    = 5000;
    }

    public function addMonitorDir(string $dir)
    {
        $this->monitorDirs[] = $dir;
        return $this;
    }
}