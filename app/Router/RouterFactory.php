<?php
declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;

/**
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class RouterFactory
{

    public static function createRouter(): RouteList
    {
        $router = new RouteList;
        $router->addRoute('<presenter>/<action>[/<id>]', 'Home:default');
        return $router;
    }
}