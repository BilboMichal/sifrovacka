<?php

namespace App\Forms;

use Nette\ComponentModel\IContainer;
use Nette\Forms\IControl;
use Nette\Forms\ISubmitterControl;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Hydrator
{
    const HYDRATE_OFF       = -1;
    const DEPTH_UNLIMITED   = -1;
    const DEPTH_UNTIL_EMPTY = -2;

    public static function hydrateIn(IContainer $container, object $data, int $depth = self::DEPTH_UNLIMITED)
    {
        if ($depth > 0) {
            $depth--;
        }

        foreach ($container->getComponents() as $control) {
            $hydrate = $name    = $control->getName();

            if (method_exists($control, 'getOptions')) {
                $options = $control->getOptions();
                if (array_key_exists('hydrateIn', $options)) {
                    $hydrate = $options['hydrateIn'];
                } elseif (array_key_exists('hydrate', $options)) {
                    $hydrate = $options['hydrate'];
                }
            }

            if ($hydrate === self::HYDRATE_OFF) {
                continue;
            }

            if ($control instanceof IControl && !$control instanceof ISubmitterControl) {
                $control->setValue(is_callable($hydrate) ? call_user_func($hydrate, $name, $data) : ($data->$hydrate ?? null));
            } elseif ($control instanceof IContainer) {
                if ($depth === self::DEPTH_UNTIL_EMPTY && !isset($data->$name)) {
                    continue;
                }
                if ($depth !== 0) {
                    static::hydrateIn($control, $data->$name, $depth);
                }
            }
        }
    }

    public static function hydrateOut(IContainer $container, object $data, int $depth = self::DEPTH_UNLIMITED)
    {
        if ($depth > 0) {
            $depth--;
        }

        foreach ($container->getComponents() as $control) {
            $hydrate = $name    = $control->getName();

            if (method_exists($control, 'getOptions')) {
                $options = $control->getOptions();
                if (array_key_exists('hydrateOut', $options)) {
                    $hydrate = $options['hydrateOut'];
                } elseif (array_key_exists('hydrate', $options)) {
                    $hydrate = $options['hydrate'];
                }
            }

            if ($hydrate === self::HYDRATE_OFF) {
                continue;
            }

            if ($control instanceof IControl && !$control instanceof ISubmitterControl) {
                $value = $control->getValue();
                if (is_callable($hydrate)) {
                    call_user_func($hydrate, $name, $data, $value);
                } else {
                    $data->$hydrate = $value;
                }
            } elseif ($control instanceof IContainer) {
                if ($depth === self::DEPTH_UNTIL_EMPTY && !isset($data->$name)) {
                    continue;
                }
                if ($depth !== 0) {
                    static::hydrateOut($control, $data->$name, $depth);
                }
            }
        }
    }
}