<?php

namespace App\Forms\Inputs;

use App\Model\Repository\Repository;
use Nette\Forms\Controls\BaseControl;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class PairsInputFactory
{
    /** @var Repository */
    private $repository;

    /** @var array */
    private $options;

    /**
     *
     * @param Repository $repository
     * @param array $options
     */
    public function __construct(Repository $repository, array $options = [])
    {
        $this->repository = $repository;
        $this->options    = $options;
    }

    protected function getPairs(string $nameProperty): array
    {
        return $this->repository->findAll()->orderBy($nameProperty)->fetchPairs('id', $nameProperty);
    }

    public function create(string $label = null, array $options = []): BaseControl
    {
        $options  = $this->options + $options;
        $multiple = $options['multiple'] ?? false;

        $items = $this->getPairs($options['nameProperty'] ?? $this->repository->getPrefixedName('name'));
        if ($multiple) {
            $control = GeneralFactories::select2Multi($label, $items, $options);
        } else {
            $control = GeneralFactories::select2($label, $items, $options);
        }

        return $control;
    }

    public function __invoke()
    {
        return $this->create(...func_get_args());
    }
}