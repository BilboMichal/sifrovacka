<?php
declare(strict_types=1);

namespace App\Forms\Inputs;

use App\Forms\Controls\AttributeDecorator;
use App\Forms\Controls\TextArea;
use App\Helpers\XmlScript;
use Nette\Http\Session;
use Nette\Http\SessionSection;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class CodeEditorInputFactory
{
    const DEFAULT_OPTIONS = [
        'mode' => 'ace/mode/xml'
    ];

    /** @var array */
    private $options;

    /** @var SessionSection */
    private $settingsSection;

    /**
     *
     * @param Session $session
     * @param array $options
     */
    public function __construct(Session $session, array $options = [])
    {
        $this->settingsSection = $session->getSection('settings');
        $this->options         = $options;
    }

    public function create(string $label = null, array $options = []): TextArea
    {
        $control = new TextArea($label);

        if (isset($this->settingsSection->editorTheme) && !isset($options['theme'])) {
            $options['theme'] = 'ace/theme/'.$this->settingsSection->editorTheme;
        }
        $options = self::DEFAULT_OPTIONS + $this->options + $options;
        $control = AttributeDecorator::setControlAttribute($control, 'ace-editor', $options);

        if ($options['mode'] === 'ace/mode/xml') {
            $control->setOption('valueIn',
                    function (?string $value): ?string {
                    return $value !== null ? XmlScript::decode($value) : null;
                })
                ->setOption('valueOut',
                    function (?string $value): ?string {
                    return $value !== null ? XmlScript::encode($value) : null;
                });
        }

        return $control;
    }

    public function __invoke()
    {
        return $this->create(...func_get_args());
    }
}