<?php

namespace App\Forms;

use App\Forms\Controls\TextArea;
use App\Helpers\OptionsTrait;
use CallbackFilterIterator;
use Iterator;
use Nette\ComponentModel\IContainer;
use Nette\Forms\IControl;
use Nette\Forms\ISubmitterControl;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
trait ContainerExtendTrait
{

    use OptionsTrait;

    abstract public function getComponents(bool $deep = false, string $filterType = null): Iterator;

    /**
     *
     * @param bool $deep
     * @return Iterator
     */
    public function getValueControls(bool $deep = false): Iterator
    {
        return new CallbackFilterIterator($this->getComponents($deep, IControl::class),
            function (IControl $control): bool {
            return !$control instanceof ISubmitterControl;
        });
    }

    /**
     *
     * @param bool $deep
     * @return Iterator
     */
    public function getSubmits(bool $deep = false): Iterator
    {
        return $this->getComponents($deep, ISubmitterControl::class);
    }

    /**
     *
     * @param bool $deep
     * @return Iterator
     */
    public function getContainers(bool $deep = false): Iterator
    {
        return $this->getComponents($deep, IContainer::class);
    }
    // Add methods override
    // ------------------------------------------------------------------

    /**
     *
     * @param type $name
     * @return self
     */
    public function addContainer($name): Container
    {
        $control               = new Container;
        $control->currentGroup = $this->currentGroup;
        if ($this->currentGroup !== null) {
            $this->currentGroup->add($control);
        }
        return $this[$name] = $control;
    }

    /**
     * Adds multi-line text input control to the form.
     * @param  string|object  $label
     */
    public function addTextArea(string $name, $label = null, int $cols = null, int $rows = null): TextArea
    {
        return $this[$name] = (new TextArea($label))
                ->setHtmlAttribute('cols', $cols)->setHtmlAttribute('rows', $rows);
    }
}