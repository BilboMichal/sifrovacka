<?php

namespace App\Forms;

use App\Helpers\OptionsTrait;
use Nette\Forms\Container as NetteContainer;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Container extends NetteContainer
{

    use ContainerExtendTrait;
}