<?php
declare(strict_types=1);

namespace App\Forms;

use Nepada\FormRenderer\Bootstrap4RendererFactory;
use Nette\Forms\Controls\BaseControl;

/**
 * Factory for Nette forms
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class FormFactory
{
    /** @var Bootstrap4RendererFactory */
    private $rendererFactory;

    /** @var callable */
    private $inputFactory;

    /**
     *
     * @return callable|null
     */
    public function getInputFactory(): ?callable
    {
        return $this->inputFactory;
    }

    /**
     *
     * @param callable $inputFactory
     * @return $this
     */
    public function setInputFactory(callable $inputFactory)
    {
        $this->inputFactory = $inputFactory;
        return $this;
    }

    /**
     *
     * @param Bootstrap4RendererFactory $rendererFactory
     */
    public function injectRenderer(Bootstrap4RendererFactory $rendererFactory)
    {
        $this->rendererFactory = $rendererFactory;
    }

    /**
     *
     * @return Form
     */
    public function create(): Form
    {
        $form = new Form;
//        $form->setHtmlAttribute('class', 'ajax');
        $form->setRenderer($this->rendererFactory->create());

        return $form;
    }

    /**
     *
     * @param string $name
     * @param mixed $args
     * @return BaseControl
     * @throws InvalidStateException
     */
    public function createInput(string $name, ...$args): BaseControl
    {
        if (!isset($this->inputFactory)) {
            throw new InvalidStateException('$inputFactory is not set');
        }

        return call_user_func($this->inputFactory, $name, ...$args);
    }

    public function __invoke(): Form
    {
        return $this->create();
    }
}