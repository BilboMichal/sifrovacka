<?php
declare(strict_types=1);

namespace App\Forms;

use Nette\Forms\Controls\BaseControl;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class FormInputFactory
{
    /** @var array */
    private $factories;

    public function __construct(array $factories = [])
    {
        foreach ($factories as $name => $factory) {
            $this->add($name, $factory);
        }
    }

    public function add(string $name, callable $factory): self
    {
        $this->factories[$name] = $factory;
        return $this;
    }

    public function create(string $name, ...$args): BaseControl
    {
        return $this->factories[$name](...$args);
    }

    public function __invoke()
    {
        return $this->create(...func_get_args());
    }
}