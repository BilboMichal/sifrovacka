(function (LiveForm) {
    const addError = LiveForm.addError;
    LiveForm.addError = function (el, message) {
        addError.call(LiveForm, el, message);
        $(el).trigger('liveForm.addError', message);
    };

    const removeError = LiveForm.removeError;
    LiveForm.removeError = function (el) {
        removeError.call(LiveForm, el);
        $(el).trigger('liveForm.removeError');
    };
}(LiveForm));
